const User = require('../../models/user');

exports.index = async (req, res) => {
    try {
        const users = await User.find({}).select('-hashed_password -salt');
        return res.status(200).json(users)
    } catch (e) {
        return res.status(500).json({error: e})
    }
};

exports.create = async (req, res) => {
    const {name, email, password, role} = req.body;
    try {
        const user = await User.findOne({email: email});
        if (user) return res.status(400).json({
            error: {
                param: 'email',
                msg: 'Email is taken'
            }
        });
        let newUser = await new User({name, email, password, role});
        await newUser.save();
        return res.status(200).json({msg: 'User created successfully'})
    } catch (e) {
        return res.status(500).json({error: e})
    }
};

exports.show = async (req, res) => {
    const userId = req.params.id;
    try {
        const user = await User.findById(userId).select('-hashed_password -salt');
        if (!user) return res.status(404).json({
            error: {
                param: 'msg',
                msg: 'Data not found'
            }
        });
        return res.status(200).json(user)
    } catch (e) {
        return res.status(500).json({error: e})
    }
};

exports.update = async (req, res) => {
    const userId = req.params.id;
    const {name, email, role} = req.body;
    try {
        const user = await User.findById(userId);
        if (!user) return res.status(404).json({
            error: {
                param: 'msg',
                msg: 'Data not found'
            }
        });

        const emailIsExists = await User.findOne({email: email, _id: {$ne: userId}});
        if (emailIsExists) return res.status(400).json({
            error: {
                param: 'email',
                msg: 'Email is taken'
            }
        });

        user.name = name;
        user.email = email;
        user.role = role;
        await user.save();

        return res.status(200).json({msg: 'User updated successfully'});
    } catch (e) {
        return res.status(500).json({error: e})
    }
};

exports.destroy = async (req, res) => {
    const userId = req.params.id;
    try {
        const user = await User.findById(userId);
        if (!user) return res.status(404).json({
            error: {
                param: 'msg',
                msg: 'Data not found'
            }
        });

        await user.delete();

        return res.status(200).json({msg: 'User deleted successfully'})
    } catch (e) {
        return res.status(500).json({error: e})
    }
};

exports.changeStatus = async (req, res) => {
    const userId = req.params.id;
    try {
        const user = await User.findById(userId);
        if (!user) return res.status(404).json({
            error: {
                param: 'msg',
                msg: 'Data not found'
            }
        });

        user.status = !user.status;
        await user.save();

        return res.status(200).json({msg: 'User status changed successfully'});
    } catch (e) {
        return res.status(500).json({error: e})
    }
};