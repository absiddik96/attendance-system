const User = require('../models/user');
const jwt = require('jsonwebtoken');

exports.signIn = (req, res) => {
  const { email, password } = req.body;
  // check if user exist
  User.findOne({ email }).exec((err, user) => {
    if (err || !user) {
      return res.status(400).json({
        error: {
          param: 'email',
          msg: `those credential doesn't exists in our record.`
        }
      });
    }
    // authenticate
    if (!user.authenticate(password)) {
      return res.status(400).json({
        error: {
          param: 'email',
          msg: `those credential doesn't exists in our record.`
        }
      });
    }
    // authenticate
    if (!user.status) {
      return res.status(400).json({
        error: {
          param: 'email',
          msg: `Your account suspended. Please contact the HR manager.`
        }
      });
    }
    // generate a token and send to client
    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, { expiresIn: '1d' });
    
    res.cookie('token', token, { expiresIn: '1d' });
    const { _id, username, name, email, role } = user;
    return res.json({
      token,
      user: { _id, username, name, email, role }
    });
  });
};

exports.signOut = (req, res) => {
  res.clearCookie('token');
  return res.json({ msg: 'Signout successfully' });
};
