const mongoose = require('mongoose');
const crypto = require('crypto');
const ROLE_TYPE = [
  {_id: 1, name: 'administrator'},
  {_id: 2, name: 'manager'},
  {_id: 3, name: 'employee'},
];

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true,
    max: 32
  },
  email: {
    type: String,
    trim: true,
    required: true,
    unique: true,
    lowercase: true
  },
  hashed_password: {
    type: String,
    required: true
  },
  salt: String,
  role: {
    type: Number,
    default: 2
  },
  status: {
    type: Boolean,
    default: true
  },
  photo: {
    type: String,
    trim: true,
  },
  resetPasswordLink: {
    data: String,
    default: ''
  }
}, { timestamps: true });

userSchema.set('toObject', { virtuals: true });
userSchema.set('toJSON', { virtuals: true });

userSchema.virtual('password').set(function (password) {
  // create a temporary variable called _password
  this._password = password;
  // generate salt
  this.salt = this.makeSalt();
  // encryptPassword
  this.hashed_password = this.encryptPassword(password);
}).get(function () {
  return this._password;
});

userSchema.virtual('role_type').get(function () {
  return ROLE_TYPE.find(role => role._id === this.role).name;
});

userSchema.statics.getRoles = function () {
  return ROLE_TYPE;
};

userSchema.methods = {
  authenticate: function (password) {
    return this.encryptPassword(password) === this.hashed_password;
  },
  
  encryptPassword: function (password) {
    if (!password) return '';
    try {
      return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
    } catch (err) {
      return '';
    }
  },
  
  makeSalt: function () {
    return Math.round(new Date().valueOf() * Math.random()) + '';
  }
};

module.exports = mongoose.model('User', userSchema);