const express = require('express');
const router = express.Router();
const {userCreateValidator, userUpdateValidator} = require("../../validators/admin/user");
const {runValidation} = require("../../validators");
const {adminMiddleware, requireSignIn} = require('../../middleware');
const {index, create, show, update, destroy, changeStatus} = require('../../controllers/admin/user');

router.get('/', requireSignIn, adminMiddleware, index);
router.post('/create', requireSignIn, adminMiddleware, userCreateValidator, runValidation, create);
router.get('/:id', requireSignIn, adminMiddleware, show);
router.put('/:id/update', requireSignIn, adminMiddleware, userUpdateValidator, runValidation, update);
router.delete('/:id/destroy', requireSignIn, adminMiddleware, destroy);
router.patch('/:id/change-status', requireSignIn, adminMiddleware, changeStatus);

module.exports = router;