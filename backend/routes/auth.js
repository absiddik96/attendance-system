const express = require('express');
const router = express.Router();
const {signIn, signOut} = require('../controllers/auth');

// validators
const {runValidation} = require('../validators');
const {userSignInValidator} = require('../validators/auth');

router.post('/sign-in', userSignInValidator, runValidation, signIn);
router.get('/sign-out', signOut);

module.exports = router;