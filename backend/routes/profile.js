const express = require('express');
const router = express.Router();
const { authMiddleware, requireSignIn } = require('../middleware');
const {profile} = require('../controllers/profile');

router.get('/profile', requireSignIn, authMiddleware, profile);

module.exports = router;