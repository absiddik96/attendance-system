const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();

// bring routes
const authRoute = require('./routes/auth');
const profileRoute = require('./routes/profile');
// admin
const userRoute = require('./routes/admin/user');

// app
const app = express();

// database
try {
  mongoose.connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  });
  console.log('Database connected')
} catch (e) {
  console.log('Database not connected')
}

// middleware
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(cookieParser());

// cors
app.use(cors({ origin: `${process.env.CLIENT_URL}` }));

// routes middleware
app.use('/api', authRoute);
app.use('/api', profileRoute);

// admin
app.use('/api/admin/user', userRoute);

// port
const port = process.env.PORT || 8000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});