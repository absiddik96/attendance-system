const {check} = require('express-validator');

exports.userCreateValidator = [
    check('name')
        .not().isEmpty().withMessage('Name is required')
        .exists({checkFalsy: true}).withMessage('Name is required'),
    check('email')
        .not().isEmpty().withMessage('Email is required')
        .exists({checkFalsy: true}).withMessage('Email is required')
        .isEmail().withMessage('Must be a valid email address'),
    check('password')
        .not().isEmpty().withMessage('Password is required')
        .exists({checkFalsy: true}).withMessage('Password is required'),
    check('role')
        .not().isEmpty().withMessage('Role is required')
        .exists({checkFalsy: true}).withMessage('Role is required')
        .isNumeric().withMessage('Role must be a numeric'),
];

exports.userUpdateValidator = [
    check('name')
        .not().isEmpty().withMessage('Name is required')
        .exists({checkFalsy: true}).withMessage('Name is required'),
    check('email')
        .not().isEmpty().withMessage('Email is required')
        .exists({checkFalsy: true}).withMessage('Email is required')
        .isEmail().withMessage('Must be a valid email address'),
    check('role')
        .not().isEmpty().withMessage('Role is required')
        .exists({checkFalsy: true}).withMessage('Role is required')
        .isNumeric().withMessage('Role must be a numeric'),
];