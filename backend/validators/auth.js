const { check } = require('express-validator');

exports.userSignInValidator = [
  check('email').isEmail().withMessage('Must be a valid email address'),
  check('password').not().isEmpty().withMessage('Password is required'),
];